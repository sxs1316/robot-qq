import json

from flask import Flask, request

from function.api import weibo_hot_rank,baidu_hot_rank,epidemic
from function.httpApi import send_private_msg, send_group_msg

app = Flask(__name__)


@app.route("/", methods=["GET", "POST"])
def main():
    data = json.loads(request.data.decode("utf-8"))
    post_type = data["post_type"]
    if post_type == "message":
        # 消息类型，群消息或私聊消息
        message_type = data["message_type"]
        # 消息ID
        message_id = data["message_id"]
        # 机器人QQ号
        robot_qq = data["self_id"]
        # 接收的消息内容
        message = data["message"]
        # 发信息的人
        sender = data["sender"]
        # 发信人昵称
        nickname = sender["nickname"]
        # 发信人QQ号
        user_id = sender["user_id"]

        # 群消息在这里处理
        if message_type == "group":
            # QQ群号
            group_id = data["group_id"]
            if message == "发送群聊消息":
                send_group_msg(group_id=group_id, message=message)
            if message == "微博热搜":
                send_group_msg(group_id=group_id, message=weibo_hot_rank())
            if message == "百度热搜":
                send_group_msg(group_id=group_id, message=baidu_hot_rank())
            if message[-2:] == "疫情":
                area = message[:-2]
                send_group_msg(group_id=group_id, message=epidemic(area))
        # 私聊消息在这里处理
        elif message_type == "private":
            if message == "发送私聊消息":
                send_private_msg(user_id=user_id, message=message)
            if message == "微博热搜":
                send_private_msg(user_id=user_id, message=weibo_hot_rank())
            if message == "百度热搜":
                send_private_msg(user_id=user_id, message=baidu_hot_rank())
            if message[-2:] == "疫情":
                area = message[:-2]
                send_private_msg(user_id=user_id, message=epidemic(area))

    return 'QQ机器人'


if __name__ == '__main__':
    app.run()
