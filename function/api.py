# --*-- coding:utf-8 --*--
# @FileName      : api.py
# @DateTime      : 2022/12/3 4:43
# @Author        : SXS
# @Email         : sxs1316@outlook.com
# @description   : 
# @Version       :
import json
import time

import requests


# 导入模板


def weibo_hot_rank(item: int = 10) -> str:
    """
    获取微博热搜排行
    :param item: 返回热搜数量，最多50
    :return: 格式化后待发送的消息字符串
    """
    url = f"https://www.aapi.icu/call/hot/weibo/?item={item}"
    res = json.loads(requests.get(url=url).text)
    message = ""
    for i in res["data"]:
        message += str(i["hot_rank"]) + ". " + i["hot_title"] + "  热度" + str(i["hot_value"]) + "\n"
    return message


def baidu_hot_rank(item: int = 10) -> str:
    """
    获取百度热搜排行
    :param item: 返回热搜数量，最多50
    :return: 格式化后待发送的消息字符串
    """
    url = f"https://www.aapi.icu/call/hot/baidu/?item={item}"
    res = json.loads(requests.get(url=url).text)
    message = ""
    for i in res["data"]:
        message += str(i["hot_rank"]) + ". " + i["hot_title"] + "  热度" + str(i["hot_value"]) + "\n"
    return message


def epidemic(area: str) -> str:
    """
    获取区域疫情情况
    :param area: 省市
    :return: 格式化后待发送的消息字符串
    """
    url = f"https://www.aapi.icu/call/es/yq/?area={area}"
    res = json.loads(requests.get(url).text)["data"]
    try:
        res = f"""{res["city"]}疫情数据：\n确诊人数：{res["confirmed"]}\n死亡人数：{res["died"]}\n累计治愈：{res["crued"]}\n新增本土无症状：{res["asymptomaticRelative"]}\n新增本土：{res["confirmedRelative"]}\n现有确诊：{res["curConfirm"]}\n更新时间：{time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(res["updateTime"])))}"""
        return res
    except KeyError:
        return "查询区域有误！"


if __name__ == '__main__':
    data = epidemic("广州")
    print(data)
