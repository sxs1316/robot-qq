# --*-- coding:utf-8 --*--
# @FileName      : httpApi.py
# @DateTime      : 2022/12/2 14:47
# @Author        : SXS
# @Email         : sxs1316@outlook.com
# @description   : 根据官网API接口重新封装
# @Version       : 1.0
import json

# 导入模板
import requests

API_HOST = "http://127.0.0.1:5700"


# 发送私聊消息
def send_private_msg(user_id: int, message: str, group_id: int = None, auto_escape: bool = False) -> dict:
    """
    发送私聊消息，返回消息ID
    :param user_id: 对方 QQ 号
    :param group_id: 主动发起临时会话群号(机器人本身必须是管理员/群主)
    :param message: 要发送的内容
    :param auto_escape: 消息内容是否作为纯文本发送 ( 即不解析 CQ 码 ) , 只在 message 字段是字符串时有效
    :return: 发送失败返回空字典，成功返回消息ID等信息
    """
    url = API_HOST + "/send_private_msg"
    data = {
        "user_id": user_id,
        "group_id": group_id,
        "message": message,
        "auto_escape": auto_escape,
    }
    res = json.loads(requests.post(url=url, data=data).text)
    if res["status"] == "ok":
        return res
    else:
        return {}


# 发送群消息
def send_group_msg(group_id: int, message: str, auto_escape: bool = False):
    """
    发送群消息
    :param group_id: QQ群号
    :param message: 要发送的内容
    :param auto_escape: 消息内容是否作为纯文本发送 ( 即不解析 CQ 码) , 只在 message 字段是字符串时有效
    :return: 发送失败返回空字典，成功返回消息ID等信息
    """
    url = API_HOST + "/send_group_msg"
    data = {
        "group_id": group_id,
        "message": message,
        "auto_escape": auto_escape,
    }
    res = json.loads(requests.post(url=url, data=data).text)
    if res["status"] == "ok":
        return res
    else:
        return {}

