# RobotQQ

#### 介绍

基于go-cqhttp+Python的QQ机器人

#### 安装教程

1.  go-cqhttp参考官方文档 [传送门](https://docs.go-cqhttp.org/)
2.  安装Python
3.  安装模块 `pip install flask`


#### 使用说明

1.  启动go-cqhttp
2.  启动flask


#### 接入接口

1.  微博热搜
2.  百度热搜
3.  疫情查询

待添加。。。
